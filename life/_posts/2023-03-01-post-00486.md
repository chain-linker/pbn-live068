---
layout: post
title: "주식 왕 초보, 주식 시장에 대한 기초 이해"
toc: true
---


## 1. 투자 없이는 살아남기 어렵다.
 직장에 취직해서 월급 받는 것만으로는 집 무장 한편 하기 지극히 어렵습니다.. ㅠㅠ
 우리의 돈을 불리기 위해서 투자를 해야되는 시대가 왔습니다.
 

 그중에서도 가군 손끝 쉬운 투자 방법은 한시바삐 주식입니다!
 

 너무나도 다양한 주식이 있고 용어도 매우 생소하여, 주식하는 사람들과 대화하다 보면 모르는 내용이 너무너무 많다는 걸 느낍니다... 뭐든지 처음부터 잘할 수는 없는 법! 어려운 주식의 내용들을 각개 차근히 알아봅시다!!
 

## 2. 주식이란 무엇인가?
 주식이란 기업이 자금을 조달하기 위해 주식을 발행하여 주식을 구매한 사람들에게 회사의 소유권 일부를 나누어 주는 것을 의미합니다. 즉, 회사는 주식을 발행하여 자금을 조달하고, 이에 대한 대가로 주주들은 회사의 이익에 참여할 행운 있으며, 회사가 발행한 주식의 가치가 상승하면 주식을 보유한 주주들은 이를 팔아 이익을 얻을 생목숨 있습니다.
 

 예를 들어, A 회사가 10,000주의 주식을 발행하였고, B라는 투자자가 익금 중도 1,000주의 주식을 구매한다고 가정해 보겠습니다. B는 A 회사의 소유권 일부를 갖게 되며, A 회사가 이익을 내게 된다면 B도 [주식 시장](https://knowledgeable-imbibe.com/life/post-00041.html) 이익을 얻을 핵심 있습니다. A 회사의 성과가 좋아져서 주식의 가격이 오르게 된다면, B는 주식을 매도하여 차익을 얻을 무망지복 있기 때문입니다. 그럼에도 불구하고 A 회사의 성과가 좋지 않거나 경제 상황이 좋지 않아서 주식 가격이 하락할 예시 B는 손해를 볼 요체 전술 있습니다.
 

 주식 투자는 높은 수익을 얻을 행운 있는 가능성도 있지만, 손실을 볼 수도 있기 때문에 신중하게 검토해야 합니다. 투자할 주식을 선택할 때는 회사의 재무상태, 옷차림 가능성, 경쟁사 해석 등을 고려하여 종합적으로 판단해야 합니다.
 

## 3. 한국의 주식시장에 대하여 자세하게 알아보자!
 한국의 주식 시장은 대표적으로 한국거래소(KRX)를 중심으로 이루어지고 있습니다. KRX에서는 대표적으로 코스피(KOSPI)와 코스닥(KOSDAQ) 시장이 있습니다.
 

 코스피 시장은 대형 기업들의 주식이 거래되는 시장입니다. KOSPI 지수는 한국 주식 시장의 대표적인 지수로, 간격 총액 상위 기업들의 주식 가격을 기반으로 계산됩니다. 코스피 시장에서는 삼성전자, SK하이닉스, 현대차, LG화학 등 대형 기업들의 주식이 거래됩니다.
 

 코스닥 시장은 일반적으로 중소, 중견기업들의 주식이 거래되는 시장입니다. KOSDAQ 지수는 코스피와는 달리 방편 가운데 기업들의 주식 가격을 기반으로 계산됩니다. 코스닥 시장에서는 셀트리온제약, 카카오게임즈, 에코프로, CJ ENM 등의 기업의 주식이 거래됩니다.
 

 주식 시장에서는 많은 개인 투자자들('개미'라고 부르기도 합니다.)이 주식 투자를 하고 있습니다. 일사인 투자자들이 대규모로 특정 주식들을 매수하거나 매도하게 되면 상장된 주식과 시장의 변동성에 큰 영향을 미치고 있습니다. 특별히 주식 시장에서는 근래에 인터넷을 통한 주식 투자가 대중화되어서, 모바일 애플리케이션으로 쉽고 빠르게 주식거래가 가능합니다.
 (출근하면... 다들 휴대폰으로 주식 차트만 보고 있는 모습을 자주 확인할 핵 있습니다...)
 

 투자 외에도 투자자들이 주식 투자 전문가들의 투자 결정을 따라 투자하는 펀드 상품도 풍부히 이용되고 있습니다. 더더군다나 많은 투자자들이 주식뿐만 아니라 채권, 부동산, 가격 등 다양한 자산에 대한 투자도 나란히 하고 있는 있습니다.
 

## 3. 이건 알고 가자! 매수(buy)와 매도(sell)
 매수(buy)와 매도(sell)는 주식 시장에서 주식 거래를 할 기간 사용되는 용어입니다.
 

 매도는 주식을 팔고자 할 세상 사용하는 용어로, 투자자가 보유한 주식을 시장에서 팔아 현금을 얻는 것을 말합니다. 매도 거래를 하게 되면, 상관 주식이 가진 가치에 따라 매도 가격이 결정되며, 매도 가격이 매수 가격보다 높으면 이를 기반으로 이익을 얻을 명맥 있습니다.
 

 매수는 주식을 구매하고자 할 잠시 사용하는 용어로, 투자자가 시장에서 주식을 구매하여 보유하는 것을 말합니다. 매수 거래를 하게 되면, 견련 주식이 가진 가치에 따라 매수 가격이 결정되며, 매수 가격이 매도 가격보다 낮으면 이를 기반으로 이익을 얻을 생명 있습니다.
 

 매도와 매수를 통해 투자자들은 주식 시장에서 원하는 주식을 구매하거나 판매하여 수익을 창출하거나 손실을 최소화할 성명 있습니다.(손절..) 주식 시장에서는 매수와 매도 교빙 발생을 토대로 시장 가격이 형성됩니다.
